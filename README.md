# Amplified restriction fragments for genomic enrichment #

The repo hosts a protocol that we commonly use for double-digest library prep of fragments for sequencing, to do genotyping by sequencing.  The method does not have an acronym or name.

The lab method was first described in this paper, but has been iteratively improved since.

Parchman TL, Gompert Z, Mudge J, Schilkey F, Benkman CW, Buerkle CA (2012) Genome-wide association genetics of an adaptive trait in lodgepole pine. Molecular Ecology, 21, 2991–3005.