\documentclass[letterpaper, 11pt]{article} 

%---------------------------------------- + ---- + ---- + ---- + ---- + ----
\setlength{\parskip}{1ex plus0.5ex minus0.5ex} 
\setlength{\textheight}{9.0in}  
\setlength{\textwidth}{6.5in} 
\setlength{\oddsidemargin}{0in}
\setlength{\topmargin}{-0.5in}
%---------------------------------------- + ---- + ---- + ---- + ---- + ----

\newlength{\dblfigwidth}
\setlength{\dblfigwidth}{\textwidth}

\newlength{\figwidth}
\setlength{\figwidth}{4in}

\setlength{\fboxsep}{3mm}
%to include figures
\usepackage{graphicx}
%to get author-year citations
\usepackage{natbib}
%%\bibpunct{[}{]}{,}{n}{}{,}

%% to include parenthetical URLs
\usepackage{url}

% for line numbering  
\usepackage{lineno}
%\usepackage{times}
\usepackage{setspace}
\usepackage{rotating}
\usepackage{float}
\usepackage{longtable}

% ---------------
% to convert from latex to html:
% htlatex filename

% to get page-numbering in upper-right corner
\pagestyle{myheadings}
\markright{}

% ---------------------------------------------------- 


\begin{document}

\noindent \begin{tabular}{p{3.5in}l}
\Large \bf Amplified restriction fragments & Tom Parchman ({\tt tparchma@uwyo.edu}) \\
\Large \bf for genomic enrichment & Zach Gompert ({\tt zgompert@uwyo.edu})\\
\bf (version 2.7 August 2017) & Liz Mandeville ({\tt emandevi@uwyo.edu}) \\
 & Alex Buerkle ({\tt buerkle@uwyo.edu})\\
 & University of Wyoming
\end{tabular}

%% updates: 
%% June 2012 - inactivation of restriction enzyme
%% October 2012 - extra PCR cycle step added.
%% July 2014 - description of Blue Pippin added
%% July 2014 - PCR pooling scheme described
%% August 2017 - PCR pooling scheme elaborated

\bigskip

This is a protocol for preparing highly-multiplexed samples for
high-throughput sequencing.  Genomic DNA is digested with restriction
enzymes, barcodes and Illumina adaptor sequences are ligated to the
digested fragments, and a subset of these fragments is amplified by
PCR.  Amplified fragments are purified and size-selected (in agarose
or with a BluePippin instrument) before being pooled into an Illumina
sequencing library (see Fig.\ \ref{schematic}).  This is a flexible
protocol that has worked with a wide variety of taxa and can be
modified to target a variable number of fragments for sequencing (see
Fig.\ \ref{expectation}). This protocol is used and described in
detail by \citet{parchman12}.

\noindent \underline{Disclaimer}: We cannot guarantee your success
with this protocol.  We also caution that this protocol and other
approaches to multiplexed sequencing lead to immense amounts of data,
with stochastic variation in sequence coverage across individuals and
genetic regions.  Importantly, we recommend making a comprehensive
plan for analyzing the data before you follow this protocol.  If you
do not know what you will do with $>10^8$ barcoded sequencing reads,
then this protocol will not be useful.


%creative commons license??
\section{Reagents and Equipment}

\begin{enumerate} \setlength{\itemsep}{0ex plus 0.1ex}
\item EcoRI (NEB, 20,000 units/mL) R0101L
\item MseI (NEB, 10,000  units/mL) R0525L
\item T4 DNA ligase (NEB, 400,000  units/mL) M0202L
\item BioRad Iproof High Fidelity DNA polymerase Cat no. 172-5301
  (\$424 for 250 $\mu$L [500 units])
\item 1 mg/mL BSA
\item 1 M NaCl
\item DMSO
\item EcoRI adaptors (see below)
\item MseI adaptors (see below)
\item Thermal Cyclers (2)
\item 96 well plates and strip caps
\item Full plate centrifuge
\item Nanodrop or similar spectrophotometer 
\item other reagents listed below
\end{enumerate}

\subsection{Items only needed if doing Gel Purification}
\begin{enumerate}
\item Molecular Biology grade agarose (suggestion: BioRad 161-3101)
\item QIAquick gel extraction kit Cat no. 28706(\$464 for 250) 
\item Agarose gel rig
\item UV light table and UV face shields
\end{enumerate}

\section{Oligonucleotide Sequences}

\subsection{Adaptors}

\subsubsection{EcoRI} \label{ecoriprep}

\begin{enumerate} 

\item See Table \ref{bcprimers} for barcoded EcoRI primer
  combinations. These adaptor sequences come in pairs and after
  annealing become one double-stranded adaptor. The barcodes residing
  in the adaptors were created using python scripts described in
  \citep{meyer10}. To create variability in the initial bases for
  Illumina sequencing, we created barcodes that were 8, 9, and 10
  bases in length. We constructed sets of barcodes so that all
  barcodes differ by a minimum of 4 bases.  This facilitates
  correction of sequencing errors in barcodes. From the total set, we
  used an additional python script to select a subset that is
  balanced in the usage of nucleotides at each position. Further
  information on the python scripts can be found at ({\tt
    http://bioinf.eva.mpg.de/multiplex/}).

\item Mix 1 $\mu$L of each oligo in a pair (100 $\mu$M stock) with 98
  $\mu$L of water to make 100 $\mu$L of 1 pmole/$\mu$L (1 $\mu$M) of
  annealed, double-stranded adaptor stock.  Heat to
  95$^{\circ}\mathrm{C}$ for 5 minutes and slowly cool to room
  temperature. This annealing step only needs to be performed once for a
  stock solution of adaptor.  Keep the set of adaptors organized in
  plate format that is convenient for later use in setting up
  reactions (e.g., Fig.\ \ref{midplate}).

\end{enumerate}

\subsubsection{MseI} \label{mseiprep}

Mix 10 $\mu$L of the MseI1 and MseI2 oligos (100 $\mu$M stock) with 80
$\mu$L of water to make 100 $\mu$L of 10 pmol/$\mu$L (10 $\mu$M)
stock.  Heat to 95$^{\circ}\mathrm{C}$ for 5 minutes and slowly cool
to room temp to anneal oligos into double-stranded adaptor.  This
annealing step only needs to be performed once for a stock solution of
adaptor.

These oligos are the same for each fragment. On an Illumina sequencer
and with our configuration of the sequencing primer, sequencing only
occurs from the EcoRI side, so there is no need to barcode the MseI
oligos.

\begin{description}  \setlength{\itemsep}{0ex plus 0.1ex}

\item[MseI1:]  5' {\tt GCAGAAGACGGCATACGAGCTCTTCCGATCTG} 3'

\item[MseI2:]  5' {\tt TACAGATCGGAAGAGCTCGTATGCCGTCTTCTGCTTG} 3'

\end{description}
 
\begin{figure}
\begin{center}
    \parbox[b]{2.4in}{\includegraphics[scale=0.4]{96well_oligos_1.eps}\\
      \includegraphics[scale=0.4]{96well_oligos_2.eps}}
    \includegraphics[scale=0.75]{96well_oligos.eps}

    \caption{Sample layouts of 96 adaptor oligos on plates.  At left
      are plates as they would be ordered.  Oligos pairs are
      combined, annealed and then stored for use as in the plate on
      the right.\label{midplate}}
\end{center}
\end{figure}


\subsubsection{PCR primers}

Mix 5 $\mu$L of the Illpcr1 and Illpcr2 oligos (100 $\mu$M stock) with
90 $\mu$L of water to make a working solution (2.5 $\mu$M of each
oligo).

The asterisks between the first three bases in each PCR primer
represent phosphothiolation (this is an option when ordering
oligos). This prevents exonucleases from acting on the ends of the
fragments, and thus protects the product prior to sequencing.

The last base of the Illpcr2 primer is a selective base and is
arbitrarily {\tt \bf G}.  Different and additional selective bases
could be added, as desired.  However, there is some evidence that the
selective base does not function with high-fidelity polymerase and
could be omitted.

\begin{description}

\item[Illpcr1 (Forward):] 
  5' {\tt A*A*TGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT} 3'

%% \item[Illpcr2 (Reverse):]%%\\
%%   5' {\tt C*A*AGCAGAAGACGGCATACGAGCTCTTCCGATCT} 3'

\item[Selective Illpcr2 (Reverse):]%%\\
  5' {\tt C*A*AGCAGAAGACGGCATACGAGCTCTTCCGATCTGTAAG} 3'

  %% ERROR -- the following oligo has incorrect bases at the 3' end
  %% (lower case), but was used for the Dec. 10 sequencing at
  %% NCGR.  \item[More selective option for Illpcr2:]%% \\ 5' {\tt
  %% C*A*AGCAGAAGACGGCATACGAGCTCTTCCGATCTcattG} 3'

\end{description}

\subsubsection{Illumina sequencing primer}

This primer is used during Illumina sequencing, is not part of the
library prep, and is presented here only to document its sequence.

5'  {\tt ACACTCTTTCCCTACACGACGCTCTTCCGATC}  3'

\section{Restriction and Ligation Reactions}

Genomic DNA is first digested with two restriction enzymes, EcoRI and
MseI. This results in a pool of fragments that have the sticky-ended
restriction cut sites on either end, and these ends provide a template
for ligation of the customized adaptor sequences. The adaptor
sequences contain the Illumina adaptors and primer sequences (and
hence provide a binding site for the Illumina PCR primers). After the
Illumina adaptor sequences on the EcoRI end, 8--10 base pair barcodes
are incorporated that allow the identification of the origin of each
sequencing read. The five bases at the 3' end of each adaptor oligo
correspond to the ligation site; that is, they match the restriction
cut site and have one added, protector base that prevents the cut from
occurring again. The restriction digest is run first, followed by the
permanent inactivation of enzymes by heating to
65$^{\circ}\mathrm{C}$. The ligation reaction is then run separately,
so digestion of adaptors containing restriction enzyme cut sites
cannot occur (a small number of our 768 EcoRI adaptor sequences
contain the EcoRI or Mse1 cut sites; likewise for any other potential
alternative restriction enzymes) and reactions can occur at their
optimum temperatures.

\subsection{Restriction Digest}
\begin{enumerate}

 

\item Place 6 $\mu$L of sample DNA in each well of a plate (DNA should
  ideally be at a minimum concentration of 20 ng/$\mu$L and a maximum
  concentration of 150 ng/$\mu$L (note: this is for small genomes, for large
  genomes add higher concentrations of DNA).  Keep on ice.

\item For each sample prepare master mix I (Table\ \ref{mmI}), mix by vortexing,
  and centrifuge. 

  For these and all other reactions make sure to prepare an excess of
  mix to accomodate multiple rounds of pipetting, particularly if you
  are working with whole plates. Because the enzymes are stored in
  glycerol and other viscous solutions, a substantial volume is lost
  through adhesion to the outside of pipette tips. We suggest making
  150\% of what you think you will need.

\item Add 3 $\mu$L of the combined master mix I to each sample. Always
  keep cold once the enzymes have been added.

\item The total reaction volume should be 9 $\mu$L. Cover and seal the
  plate, vortex, centrifuge and incubate at 37 $^{\circ}\mathrm{C}$
  for 8 hours, followed by 65$^{\circ}\mathrm{C}$ for 45 minutes (to
  inactivate the enzyme) on a thermalcycler with a heated lid.
\end{enumerate}

\begin{table}[h]
  \begin{center}
    \begin{tabular}{|l|c|}   \hline
      & \multicolumn{1}{c|}{Number of samples}\\
      Reagent & 1$\times$  \\ \hline
      10$\times$ T4 Buffer & 1.15  \\
      1M NaCl & 0.60   \\
      1 mg/mL BSA & 0.60  \\ 
      Water & 0.25  \\  
      MseI & 0.12   \\
      EcoRI & 0.28  \\ \hline

    \end{tabular}
    \caption{\label{mmI} Reagents and volumes for Restriction Digest master mix
      I (3 $\mu$L prepared per sample).}
  \end{center}
\end{table}

\subsection{Adaptor Ligation}
\begin{enumerate}
\item Thaw MseI and EcoRI adaptors. Have these adaptors annealed and
  easily accessible in plate format (for the EcoRI adaptors) (see
  Sections \ref{ecoriprep} and \ref{mseiprep}).

\item Add 1.4 $\mu$L of master mix II to each restriction-digested reaction.

\item Add 1 $\mu$L of EcoRI adaptors to each well on plate.  Note that
  the MseI adaptor is in master mix I.

\item The total reaction volume should now be 11.4 $\mu$L. Cover and
  seal the plate, vortex, centrifuge and incubate at 16
  $^{\circ}\mathrm{C}$ for 6 hours on a thermalcycler.

\item Dilute the Restriction-Ligation reaction with 189 $\mu$L of
  0.1$\times$ TE. Store at 4 $^{\circ}\mathrm{C}$ for a month, or -20
  $^{\circ}\mathrm{C}$ for longer.

\end{enumerate}


\begin{table}[H]
 \begin{center}
  \begin{tabular}{|l|c|} \hline
    & \multicolumn{1}{c|}{Number of samples}\\
    Reagent & 1$\times$     \\	
    \hline
      MseI adaptor & 1  \\ 
      Water & 0.072 \\
      10$\times$ T4 buffer & 0.1   \\
      1M NaCl & 0.05  \\ 
      1 mg/mL BSA & 0.05  \\ 
      T4 DNA ligase & 0.1675 \\ \hline   
  \end{tabular}
  \caption{\label{mmII} Reagents and volumes for Restriction-Ligation master mix
    II (1.4  $\mu$L prepared per sample).}
 \end{center}
\end{table}

\section{ PCR amplification}
\begin{enumerate}

\item This PCR step uses the Illumina PCR primers to amplify fragments
  that have our adaptors+barcodes ligated onto the ends. To ameliorate
  stochastic differences in PCR production of fragments in reactions,
  we run two separate 20 $\mu$L reactions per restriction-ligation
  product, and later combine them.

  Restriction-Ligation products can be pooled as template for the PCR,
  to save on the cost of Taq.  Various projects have pooled R/L
  product from 2--4 individuals.  For some work with consistently high
  DNA template concentrations, all 768 R/L products were pooled
  (corresponding to the total number of unique EcoRI adaptors, or
  ``barcodes''; this has worked for Liz Mandeville's work with fish),
  and reactions were replicated several times (not just twice, as
  above).  It is likely that a conservative approach for uneven
  template concentrations would be to not pool (particularly if some
  are very low), or only pool only a few individuals.  Regardless of
  pooling, it is likely beneficial to perform replicate
  amplifications from R/L templates.  Following ligation, each
  individual is uniquely labeled, so pooling templates for PCR will not
  introduce contamination.


\begin{table}[H]
 \begin{center}
  \begin{tabular}{|l|cc|}
   \hline
   Reagent  & 20  $\mu$L Reaction & 40  $\mu$L Reaction  \\ \hline
   Water & 9.67 & 19.33    \\
   5$\times$ Iproof buffer & 4.0 & 7.99  \\
   dNTP (10 mM)  & 0.4 & 0.8   \\ 
   MgCl$_2$ (50mM) & 0.4 & 0.8  \\ 
   Pre-mixed PCR Primers, 5 $\mu$M & 1.33 & 2.66  \\
   Iproof taq & 0.2 & 0.4   \\ 
   DMSO & 0.15 & 0.30 \\
   R/L product & 4 & 8   \\ \hline
  \end{tabular}
  \caption{Reagents and volumes for PCR amplification reactions.}
 \end{center}
\end{table}

\item Thermalcycler profile for this PCR: 98$^{\circ}\mathrm{C}$ for
  30s; 30 cycles of: 98$^{\circ}\mathrm{C}$ for 20s,
  60$^{\circ}\mathrm{C}$ for 30s, 72$^{\circ}\mathrm{C}$ for 40s; final
  extension at 72$^{\circ}\mathrm{C}$ for 10 min.

%consider removing final extension.

\end{enumerate}

\section{Extra PCR step}
\begin{enumerate}

\item This PCR step adds Illumina PCR primers, dNTPs, and iproof
  buffer and executes a final long PCR cycle. The purpose of this step
  is to attempt to convert single stranded template remaining from the
  first PCR to double stranded. This typically results in libraries
  that have a better distribution of fragments in the correct size
  range, presumably because it removes single stranded molecules that
  interfere with the precision of the electrophoresis step below.

\item Add 2.125 $\mu$L of the mix below directly to each reaction
  immediately after the PCR step above is completed.

\begin{table}[H]
 \begin{center}
  \begin{tabular}{|l|c|}
   \hline
   Reagent  & 20  $\mu$L Reaction \\ \hline
   5$\times$ Iproof buffer & 0.425 \\
   dNTP (10 mM)  & 0.4 \\ 
   Pre-mixed PCR Primers, 5 $\mu$M & 1.33   \\ \hline
  \end{tabular}
  \caption{Reagents and volumes for PCR optional final PCR cycle.}
 \end{center}
\end{table}

\item Thermalcycler profile for this PCR cycle: 98$^{\circ}\mathrm{C}$ for
  3 min; 60$^{\circ}\mathrm{C}$ for 2 mins, and 72$^{\circ}\mathrm{C}$ for 10 min.

\item Note that we are still in the process of determining empirically
  how well this extra step works.

%consider removing final extension.

\end{enumerate}

\section{Blue Pippin size selection (Option 1, preferred)}

This step reduces the template for sequencing by selecting a desired
size range of DNA fragments, and is usually done by the sequencing
facility. Note that this step replaces Gel Purification as our
preferred method for size-selection of DNA fragments for the final
sequencing library. We prefer Blue Pippin for two reasons. First,
Bioanalyzer results suggest that Blue Pippin achieves cleaner size
selection. Second, Blue Pippin is more reproducible than the Gel
Purification methods that we previously used.

%% Reused some of the Gel Purification text. 
Blue Pippin works in a similar way to typical gel electrophoresis, but
uses standardized gel cassettes and automated size selection, rather
than a hand-poured gel and manual excision of fragments from a gel. It
is necessary to specify a size range of fragments that you would like
represented in the final genomic library for sequencing. The target
size range can be adjusted to modify the number of fragments expected
for sequencing. Because techniques such as this typically result in a
negative relationship between fragment size and number, selecting
larger fragments should decrease the final number of fragments in the
template and increase the coverage depth of these regions after
sequencing.  Similarly, decreasing the size interval selected will
also reduce the number of fragments and increase coverage. The number
of fragments produced is also in part a function of genome size, so
awareness of your organism's genome size is helpful in making an
appropriate choice for size selection (Fig.\ \ref{expectation}). We have typically used a
50--100 bp range somewhere between 250 and 500 bp in total fragment
length (meaning that we might select, as an example, fragments that
are 300--400 bp in length).


\section{Gel Purification size selection (Option 2)}

This step involves cutting a desired size range of fragments out of an
agarose gel. The size range cut out of the gel can be adjusted to
modify the number of fragments expected for sequencing. Because
techniques such as this typically result in a negative relationship
between fragment size and number, cutting out larger fragments should
decrease the final number of fragments in the template and increase
the coverage depth of these regions after sequencing. To eliminate
variation in the size and amount of agarose punched out for each
sample, it is recommended to pool all PCR reactions (they already have
the barcodes incoorporated) before loading the gel.  The pooled
library can then be run in as many wells on the gel as necessary. We
are running 96 samples in replicate on two plates, pooling all of the
PCR product, and then running approximately 10 lanes on a single
agarose gel.  If desired, allowing PCR product to evaporate on the
bench top for several hours (or overnight) concentrates the PCR
product, and means that fewer lanes are required on the gel to
accommodate all of the samples (however, note that gels can be
overloaded with concentrated DNA and lead to anomalous migration in
the gel).

\begin{enumerate}

\item Run the PCR product out on 2.5\% agarose gel at 100 volts for
  2--3 hours (adjust voltage and time as needed for different gel
  boxes). It is important to include a good 100 bp or 50 bp ladder at
  multiple spots on the gel so that a clear line can be visualized
  across the gel at a consistent size. Adding ethidium bromide into
  the gel (rather than staining after electrophoresis) reduces time
  and can improve consistency, and will not interfere with PCR after
  gel purification. We have been adding 9 $\mu$L of ethidium bromide
  per 200 mL of prepared gel. Be very careful that every gel is
  standardized as much as possible (same voltage, time, agarose
  concentration, etc.) in the case that multiple gels are prepped for
  a single library or project. This is necessary to ensure that the
  same size range is cut on every gel. Because PCR product can be
  entirely pooled for a given library, the most efficient way to
  proceed is to run 8--10 lanes of the pooled library on a thick gel,
  so 70--80 ul can be loaded into each well. This results in
  sufficient final library concentration and volume for Illumina
  sequencing (with ample excess).

\item Cut the desired region out of the gel using the large end of
  sterile 1000 $\mu$L pipette tips.  The region between 300--400 bp
  seems to be a good choice here and is recommended. Because of the
  potentially large amount of sequence representing Illumina PCR
  primers in the products, cut out fragments that are at least 250
  bases in length. Because cutting out regions of a gel can take
  some time, be sure to use face and eye UV protection. In addition,
  realize that long periods of UV exposure to the DNA in the gel will
  have a mutagenizing effect. Therefore, minimize the amount of time
  that the gel is exposed to the UV.

\item Purify the excised gel punches using Qiaquick gel purification
  kits.

\end{enumerate}

\section{Preparing final template for Illumina sequencing}

\subsection{If using Blue Pippin}
For Blue Pippin, you will submit pooled PCR product without any
further modification. Prior to shipping, check the concentration of
the PCR product using a Nanodrop or similar spectrophotometer. DNA
concentrations in PCR product are high (we have seen concentrations of
300-700 ng/$\mu$L using this protocol). Sequencing facilities
typically request no more than 500 $\mu$L (UT-GSAF requests 100
$\mu$L). We recommend shipping libraries overnight on dry ice, in
Safelock vials wrapped tightly in parafilm. Different sequencing
facilities have different shipping preferences, so be sure to check
specific requirements before shipping.


\subsection{If using Gel Purification}
Use a Nanodrop or similar spectrophotometer to measure the DNA
concentration of purified gel punch. Add the same amount of DNA from
each gel purification to a 2 mL tube.  A total concentration of
25 ng/$\mu$L -- 100 ng/$\mu$L is ideal for sending off for Illumina
sequencing, although library size requirements vary. We are taking
10--50 $\mu$L from each of the 10 purified gel punches and combining
this into a pool to submit for sequencing.  NCGR does not want to
handle more than 500 $\mu$L, so submit samples in 500 $\mu$L tubes.

\bibliography{evolgen} 
\bibliographystyle{mol-ecol}


\begin{sidewaysfigure}
  \begin{center}
    \includegraphics[scale=0.7]{illuminaPrep.eps}
    \caption{A schematic of the steps involved in the
      Restriction-Ligation and PCR steps involved in creating the
      seqeuncing library. \label{schematic}}
  \end{center}
\end{sidewaysfigure}


\newpage

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.8]{frag_genomesize.eps}
    \caption{Number of fragments (200--500 bp) that are expected to be
      generated by digesting genomes of differing size.  These
      expectations were derived by using Perl to digest reference
      genomes for \emph{Drosophila}, chicken, mouse,
      \emph{Arabidopsis}, \emph{Tribolium} and human, and to count
      fragments within the 200--500 bp size range. In practice, a
      larger number of fragments is recovered and assembled in
      population studies.  \label{expectation} }
  \end{center}
\end{figure}

\setcounter{table}{0}
\renewcommand{\thetable}{S\arabic{table}}

\begin{longtable}{ll}
\caption{\label{bcprimers} Example EcoRI adaptor sequences with barcodes
  added. The barcodes (in uppercase letters) are located between the
  adaptor sequence and the restriction site sequence (see
  Fig.\ \ref{schematic}). The full list of barcodes can be found in
  the supplemental file annealed\_barcode\_out\_768.csv}\\  \hline
Name  & Sequence (5' to 3')  \\	 \hline
\endfirsthead
Name  & Sequence (5' to 3')  \\	 \hline
\endhead
Mid1.1  & {\tt ctctttccctacacgacgctcttccgatctACGAGTGCGTc} \\
MID1.2 & {\tt aattgACGCACTCGTagatcggaagagcgtcgtgtagggaaagagtgt} \\
MID2.1  & {\tt ctctttccctacacgacgctcttccgatctACGCTCGACAc} \\
MID2.2 & {\tt aattgTGTCGAGCGTagatcggaagagcgtcgtgtagggaaagagtgt} \\
MID3.1  & {\tt ctctttccctacacgacgctcttccgatctAGACGCACTCc} \\
MID3.2 & {\tt aattgGAGTGCGTCTagatcggaagagcgtcgtgtagggaaagagtgt} \\
MID4.1  & {\tt ctctttccctacacgacgctcttccgatctAGCACTGTAGc} \\
MID4.2 & {\tt aattgCTACAGTGCTagatcggaagagcgtcgtgtagggaaagagtgt} \\
MID6.1  & {\tt ctctttccctacacgacgctcttccgatctATATCGCGAGc} \\
MID6.2 & {\tt aattgCTCGCGATATagatcggaagagcgtcgtgtagggaaagagtgt} \\
MID7.1  & {\tt ctctttccctacacgacgctcttccgatctCGTGTCTCTAc} \\
MID7.2 & {\tt aattgTAGAGACACGagatcggaagagcgtcgtgtagggaaagagtgt} \\
MID8.1  & {\tt ctctttccctacacgacgctcttccgatctCTCGCGTGTCc} \\
MID8.2 & {\tt aattgGACACGCGAGagatcggaagagcgtcgtgtagggaaagagtgt} \\
MID10.1  & {\tt ctctttccctacacgacgctcttccgatctTCTCTATGCGc} \\
MID10.2 & {\tt aattgCGCATAGAGAagatcggaagagcgtcgtgtagggaaagagtgt} \\

 \hline
\end{longtable}


\end{document}


